# postfix-maillog-monitor

Postfix maillog parser daemon (in php :( ), with a UI (in JS, using meteor), 
for watching maillog events, and tracing maillog event details.

New events are automatically pushed to the browser for both detail view, and
status view.


## Prereq's:


Datastore: mongodb

- Nothing special here

UI: meteor

- iron-router

- bootstrap

- jquery

Parser: php

- inotify

- mongo


## How to run:

1. Start parsing daemon:

    `php maillog.php &`


2. Start meteor:

    `export MONGO_URL=mongodb://localhost:27017/meteor`
    
    `cd postfix-maillog-monitor/maillogapp; meteor`


3. Open Browser:

    `open http://localhost:3000`