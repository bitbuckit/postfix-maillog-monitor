// Open connection to mongodb collections
Entries = new Meteor.Collection("logentries");
Message = new Meteor.Collection("logentriesarchive");

// Routes
Router.map(function() {
    this.route('maillog', {
        path: '/',
        data: {
            logentries : function() { return Entries.find({}, {sort: {date: -1}, limit: 20}); }
        }
    });

    this.route('traceMessage', {
        path: '/message/:_id',
        data: function(){
            var _id = this.params._id;
            templateData = {
                messageDetail: function() {
                    return Message.find({'messageid':_id}, {sort:{date: 1}});
                }
            }
            return templateData;
        }
    });
});

// Client
if (Meteor.isClient) {
  Template.maillog.events({
    'click tr': function (event, template) {
      window.location.href = '/message/' + event.currentTarget.title;
    }
  });
}

// Server
if (Meteor.isServer) {
  Meteor.startup(function () {
  });
}
