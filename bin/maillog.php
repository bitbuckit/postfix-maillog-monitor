#!/usr/bin/php

/*
 * maillog.php - Postfix maillog parser daemon, writes to mongodb collection
 *
 * Compatibility: unix only
 *
 * Requires: inotify & mongo:
 *
 *   $ sudo pecl install inotify
 *   $ sudo pecl install mongo
 *
 */


// This is the location of the maillog to watch
$file = 'maillog2';

// Initialize lastpos cursor
$lastpos = 0;

// Outer loop: Ensures that when log is rotated we pick up the new logfile
while (true) {

    // Inner loop
    while (true) {

        $ret = tail($file, $lastpos);

        if ($ret) {
            parse_data($ret);
        }
        else {
            sleep(2);
        }
    }

    // We should never reach here, but if we do, sleep for 5 seconds and continue;
    sleep(5);
}



/**
 * Parses data as read from the maillog
 *
 * Handles 2 cases using the same pattern, for *smtp* and *qmgr* processes as follows:
 *
 * - single recip:
 * 
 * Mar 31 06:00:06 Proxy-1 postfix/qmgr[1588]: B86CE402F4: from=<xxx@xxx.com>, size=11057, nrcpt=1 (queue   active)
 * Mar 31 06:00:07 Proxy-1 postfix/smtp[7968]: B86CE402F4: to=<xxx@xxx.com>, relay=xxx.com[123.123.123.123]:25, delay=0.35, delays=0/0.01/0.11/0.23, dsn=2.0.0, status=sent (250 OK)
 * |-------------| |-----| |----------------|  |--------|  |--------------------...EOL
 * ^1              ^2      ^3                  ^4          ^5
 * 
 * - multi recip:
 * 
 * Mar 31 06:00:01 Proxy-1 postfix/qmgr[1588]: E45E040153: from=<xxx@xxx.com>, size=168447, nrcpt=2 (queue active)
 * Mar 31 06:00:02 Proxy-1 postfix/smtp[7966]: E45E040153: to=<xxx@xxx.com>, relay=xxx.com[123.123.123.123]:25, delay=0.56, delays=0.03/0.01/0.02/0.51, dsn=2.0.0, status=sent (250 Ok: queued as 741386C46E9)
 * Mar 31 06:00:02 Proxy-1 postfix/smtp[7966]: E45E040153: to=<xxx@xxx.com>, relay=xxx.com[123.123.123.123]:25, delay=0.56, delays=0.03/0.01/0.02/0.51, dsn=2.0.0, status=sent (250 Ok: queued as 741386C46E9)
 * |-------------| |-----| |----------------|  |--------|  |--------------------...EOL
 * ^1              ^2      ^3                  ^4          ^5
 * 
 * 1: Date 
 * 2: Hostname
 * 3: Process 
 * 4: MessageID 
 * 5: Log Detail 
 * 
 * 
 * - Here's a warning, we'll skip these:
 *
 * Mar 30 04:00:13 Proxy-1 postfix/smtp[13796]: warning: no MX host for borat.com has a valid address record 
 * 
 * 
 * @param string $data - data read from file
 * @return string
 */
function parse_data($data) {

    $matches;
    $pattern  = '/';
    $pattern .= '(\w+\s+\d+\s+\d+:\d+:\d+)\s+';
    $pattern .= '([\w-]+)\s+';
    $pattern .= '([a-z0-9\/_\[\]-]+):\s+';
    $pattern .= '(\w+):\s+';
    $pattern .= '(.*)$';
    $pattern .= '/i';

    $lines = preg_split('/(?:\r\n|\n|\r)/', $data);

    foreach ($lines as $line) {

        // Ensure $line is one of postfix/qmgr or postfix/smtp
        if (!preg_match('/postfix\/(qmgr|smtp)/', $line)) {
            continue;
        }

        // Skip to next line if $line does not match pattern
        if (!preg_match($pattern, $line, $matches)) {
            continue;
        }

        // Storate for parsed items in $line
        $items = array();

        $items["log_msg"]   = trim($line);
        $items["date"]      = trim($matches[1]);
        $items["host"]      = trim($matches[2]);
        $items["process"]   = trim($matches[3]);
        $items["messageid"] = trim($matches[4]);
        $details            = trim($matches[5]);

        // Ensure we have a message id or skip this log item, as we use
        // message id as a key
        if (empty($items["messageid"]) || $items["messageid"] == 'warning') {
            continue;
        }

        $fields_array = explode(',', $details);

        // Iterate over items in the log detail
        foreach ($fields_array as $item) {

            list($k, $v) = array_pad(explode('=', $item, 2), 2, null);

            $k = trim($k);
            $v = trim($v);

            // Skip to next item if value is null
            if (empty($v)) { continue; }

            // User lowercase keys
            $k = strtolower($k);

            // Ensure status only includes sent, deferred, bounced.
            if ($k == 'status') {
                $v = explode(' ', trim($v));
                $v = $v[0];
            }

            // Remove angle braces from addresses, unless address is blank
            if ($k == 'to' || $k == 'from') {
                $v = preg_replace("/^<([^>]+)>/", "$1", $v);
            }

            if (empty($items[$k])) {
                $items[$k] = $v;
            }
            else {
                array_push($items[$k], $v);
            }
        }

        // Connect to mongodb
        $m = new MongoClient();

        // Select logentries collection, which is our status resource
        $collection = $m->selectCollection('meteor', 'logentries');

        //$collection->runCommand('convertToCapped',array('size'=>100000));

        // Update or insert document
        $collection->update(
            array("messageid" => $items["messageid"]),
            array('$set'      => $items),
            array("upsert"    => true)
        );
        
        // Select logentriesarchive collection, where we store every message
        // transaction details
        $collection = $m->selectCollection('meteor', 'logentriesarchive');
        $collection->insert($items);
    }

    return true;
}


/**
 * Watch the maillog file for changes using inotify and return the changed data
 *
 * @param string $file - filename of the file to be watched
 * @param integer $pos - actual position in the file
 * @return string
 */
function tail($file, &$pos, $include_all = null) {

    if (!file_exists($file)) return;

    // Set the size of the file
    if(!$pos && !$include_all) $pos = filesize($file);

    // Open an inotify instance
    $fd = inotify_init();

    // Watch $file for changes.
    $watch_descriptor = inotify_add_watch($fd, $file, IN_ALL_EVENTS);

    // Loop forever (breaks are below)
    while (true) {

        // Read events (blocking call)
        $events = inotify_read($fd);

        // Loop though the events which occured
        foreach ($events as $event => $evdetails) {

            // React on the event type
            switch (true) {

                // File was modified
                case ($evdetails['mask'] & IN_MODIFY):

                    // Stop watching $file for changes
                    inotify_rm_watch($fd, $watch_descriptor);

                    // Close the inotify instance
                    fclose($fd);

                    // Open the maillog
                    $fp = fopen($file,'r');
                    if (!$fp) return false;

                    // Seek to the last EOF position
                    fseek($fp,$pos);
                    $buf = '';

                    // Read until EOF
                    while (!feof($fp)) {
                        $buf .= fread($fp,8192);
                    }

                    // Save the new EOF to $pos
                    $pos = ftell($fp); // (remember: $pos is called by reference)

                    // Close the file pointer
                    fclose($fp);

                    // Return the new data and leave the function
                    return $buf;

                break;

                // File was moved or deleted
                case ($evdetails['mask'] & IN_MOVE):
                case ($evdetails['mask'] & IN_MOVE_SELF):
                case ($evdetails['mask'] & IN_DELETE):
                case ($evdetails['mask'] & IN_DELETE_SELF):

                    // Stop watching $file for changes
                    inotify_rm_watch($fd, $watch_descriptor);

                    // Close the inotify instance
                    fclose($fd);

                    // Reset pos
                    $pos = 0;

                    // Return a failure
                    return false;
                break;
            }
        }
    }
}

